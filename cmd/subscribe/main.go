package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/nats-io/nats.go"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	natsreqresponse "gitlab.com/searsaw/nats-req-response"
	"gitlab.com/searsaw/nats-req-response/pkg/utils"
)

var (
	MaxReconnectAttempts = 5
)

func main() {
	natsUrl := flag.String("nats-url", nats.DefaultURL, "url of the NATS server")
	port := flag.String("port", "8080", "port of the HTTP server")
	flag.Parse()

	conn, err := utils.TryNatsConnect(*natsUrl, "number-receiver", MaxReconnectAttempts)
	if err != nil {
		fmt.Printf("error connecting to NATS: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("connected to NATS")
	defer conn.Close()

	encodedConnection, err := nats.NewEncodedConn(conn, nats.JSON_ENCODER)
	if err != nil {
		fmt.Printf("error creating an encoded connection: %s\n", err)
		os.Exit(1)
	}

	rand.Seed(time.Now().Unix())

	_, err = encodedConnection.QueueSubscribe("multiply", "receiver", func(subject, reply string, request *natsreqresponse.Message) {
		fmt.Printf("processing request to multiply %d\n", request.Data.Number)
		number := request.Data.Number * (rand.Intn(20) + 1)
		response := natsreqresponse.Message{
			Status: natsreqresponse.Success,
			Data: natsreqresponse.Data{
				Number: number,
			},
		}

		if err := encodedConnection.Publish(reply, &response); err != nil {
			fmt.Printf("error sending the response: %s", err)
		}
	})
	if err != nil {
		fmt.Printf("error subscribing to the queue: %s\n", err)
		os.Exit(1)
	}

	if err := encodedConnection.Flush(); err != nil {
		fmt.Printf("error flushing to the queue: %s\n", err)
		os.Exit(1)
	}

	http.Handle("/metrics", promhttp.Handler())

	fmt.Printf("the server is running on :%s\n", *port)
	if err := http.ListenAndServe(":"+*port, nil); err != http.ErrServerClosed {
		fmt.Printf("error starting the server: %s\n", err)
		os.Exit(1)
	}

	if err := encodedConnection.Drain(); err != nil {
		fmt.Printf("error draining the NATS connection: %s\n", err)
		os.Exit(1)
	}
	fmt.Println("the server has shutdown successfully")
}
