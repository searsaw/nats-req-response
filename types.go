package natsreqresponse

type Status string

var (
	Success Status = "success"
	Error   Status = "error"
)

type Message struct {
	Status Status `json:"status"`
	Data   Data   `json:"data"`
}

type Data struct {
	Number int    `json:"number,omitempty"`
	Error  string `json:"error,omitempty"`
}
