package utils

import (
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
)

func TryNatsConnect(url, name string, maxAttempts int) (*nats.Conn, error) {
	var outErr error
	for i := 0; i < maxAttempts; i++ {
		fmt.Printf("connecting to NATS servers at %s\n", url)
		conn, err := nats.Connect(url, nats.Name(name), nats.Timeout(2*time.Second))
		if err != nil {
			outErr = err
			time.Sleep(5 * time.Second)
			continue
		}
		return conn, nil
	}
	return nil, outErr
}
